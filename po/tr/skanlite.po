# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Ahmet Emre Aladağ <emre@emrealadag.com>, 2008.
# H. İbrahim Güngör <ibrahim@pardus.org.tr>, 2011.
# obsoleteman <tulliana@gmail.com>, 2008-2009,2012.
# Volkan Gezer <volkangezer@gmail.com>, 2013-2014, 2017, 2021.
msgid ""
msgstr ""
"Project-Id-Version: extragear-graphics-kde4\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-12-21 02:07+0000\n"
"PO-Revision-Date: 2021-03-23 12:40+0100\n"
"Last-Translator: Volkan Gezer <volkangezer@gmail.com>\n"
"Language-Team: Turkish <kde-l10n-tr@kde.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.08.2\n"

#: ImageViewer.cpp:42
#, kde-format
msgid "Zoom In"
msgstr "Büyüt"

#: ImageViewer.cpp:45
#, kde-format
msgid "Zoom Out"
msgstr "Küçült"

#: ImageViewer.cpp:48
#, kde-format
msgid "Zoom to Actual size"
msgstr "Özgün Boyuta Dön"

#: ImageViewer.cpp:51
#, kde-format
msgid "Zoom to Fit"
msgstr "Ekrana Sığdır"

#: main.cpp:30 skanlite.cpp:780
#, kde-format
msgid "Skanlite"
msgstr "Skanlite"

#: main.cpp:32
#, kde-format
msgid "Scanning application by KDE based on libksane."
msgstr "KDE tarafından libksane tabanlı bir tarama uygulaması."

#: main.cpp:34
#, kde-format
msgid "(C) 2008-2020 Kåre Särs"
msgstr "(C) 2008-2020 Kåre Särs"

#: main.cpp:39
#, kde-format
msgid "Kåre Särs"
msgstr "Kåre Särs"

#: main.cpp:40
#, kde-format
msgid "developer"
msgstr "geliştirici"

#: main.cpp:43
#, kde-format
msgid "Gregor Mi"
msgstr "Gregor Mi"

#: main.cpp:44 main.cpp:47
#, kde-format
msgid "contributor"
msgstr "katkıcı"

#: main.cpp:46
#, kde-format
msgid "Arseniy Lartsev"
msgstr "Arseniy Lartsev"

#: main.cpp:49
#, kde-format
msgid "Gilles Caulier"
msgstr "Gilles Caulier"

#: main.cpp:50 main.cpp:56
#, kde-format
msgid "Importing libksane to extragear"
msgstr "libksane'i , extragear'a aktarmak"

#: main.cpp:52
#, kde-format
msgid "Anne-Marie Mahfouf"
msgstr "Anne-Marie Mahfouf"

#: main.cpp:53
#, kde-format
msgid "Writing the user manual"
msgstr "Kullanım kılavuzu yazımı"

#: main.cpp:55
#, kde-format
msgid "Laurent Montel"
msgstr "Laurent Montel"

#: main.cpp:58
#, kde-format
msgid "Chusslove Illich"
msgstr "Chusslove Illich"

#: main.cpp:59 main.cpp:62
#, kde-format
msgid "Help with translations"
msgstr "Çevirilerde yardımcı"

#: main.cpp:61
#, kde-format
msgid "Albert Astals Cid"
msgstr "Albert Astals Cid"

#: main.cpp:65
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Ahmet Emre Aladağ"

#: main.cpp:66
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "emre@emrealadag.com"

#: main.cpp:74
#, kde-format
msgid "Sane scanner device name. Use 'test' for test device."
msgstr "Sane tarayıcı aygıt adı. Test aygıtı için 'test' kullanın."

#: main.cpp:74
#, kde-format
msgid "device"
msgstr "aygıt"

#. i18n: ectx: property (windowTitle), widget (QDialog, SaveLocation)
#: SaveLocation.ui:20
#, kde-format
msgid "Save Location"
msgstr "Kaydetme Konumu"

#. i18n: ectx: property (text), widget (QLabel, locationLabel)
#. i18n: ectx: property (text), widget (QLabel, label)
#: SaveLocation.ui:28 settings.ui:80
#, kde-format
msgid "Save Location:"
msgstr "Kaydetme Konumu:"

#. i18n: ectx: property (text), widget (QLabel, namePrefixLabel)
#: SaveLocation.ui:55
#, kde-format
msgid "Name & Format:"
msgstr "Ad & Biçim:"

#. i18n: ectx: property (text), widget (QLabel, numberLabel)
#. i18n: ectx: property (text), widget (QLabel, label_3)
#: SaveLocation.ui:74 settings.ui:112
#, kde-format
msgid "####."
msgstr "####."

#. i18n: ectx: property (text), widget (QLabel, numStartFromLabel)
#: SaveLocation.ui:86
#, kde-format
msgid "Start numbering from:"
msgstr "Numaralandırmaya şuradan başla:"

#. i18n: ectx: property (text), widget (QLabel, u_resultLabel)
#: SaveLocation.ui:123
#, kde-format
msgid "Example:"
msgstr "Örnek:"

#. i18n: ectx: property (text), widget (QLabel, u_resultValue)
#: SaveLocation.ui:130
#, kde-format
msgid "/home/joe/example.png"
msgstr "/home/cemal/örnek.png"

#. i18n: ectx: property (windowTitle), widget (QWidget, SkanliteSettings)
#: settings.ui:14 skanlite.cpp:61
#, kde-format
msgid "Settings"
msgstr "Ayarlar"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: settings.ui:20
#, kde-format
msgid "Image saving"
msgstr "Resim kaydetme"

#. i18n: ectx: property (text), widget (QLabel, label_7)
#: settings.ui:26
#, kde-format
msgid "Preview before saving:"
msgstr "Kaydetmeden önce önizle:"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: settings.ui:46
#, kde-format
msgid "Save mode:"
msgstr "Kaydetme kipi: "

#. i18n: ectx: property (text), item, widget (QComboBox, saveModeCB)
#: settings.ui:60
#, kde-format
msgid "Open the save dialog for every image"
msgstr "Her resim için kaydetme penceresini aç"

#. i18n: ectx: property (text), item, widget (QComboBox, saveModeCB)
#: settings.ui:65
#, kde-format
msgid "Open the save dialog for the first image only"
msgstr "Sadece ilk resim için kaydetme penceresini aç"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: settings.ui:90
#, kde-format
msgid "Name && Format:"
msgstr "İsim ve Biçim:"

#. i18n: ectx: property (text), widget (QLineEdit, imgPrefix)
#: settings.ui:105
#, kde-format
msgid "Image"
msgstr "Görüntü"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: settings.ui:124
#, kde-format
msgid "Specify save quality:"
msgstr "Kaydetme kalitesini belirt:"

#. i18n: ectx: property (suffix), widget (QSpinBox, imgQuality)
#: settings.ui:192
#, no-c-format, kde-format
msgid "%"
msgstr "%"

#. i18n: ectx: property (title), widget (QGroupBox, generalGB)
#: settings.ui:233
#, kde-format
msgid "General"
msgstr "Genel"

#. i18n: ectx: property (text), widget (QCheckBox, setPreviewDPI)
#: settings.ui:239
#, kde-format
msgid "Set preview resolution (DPI)"
msgstr "Önizleme çözünürlüğünü ayarla (DPI)"

#. i18n: ectx: property (text), item, widget (QComboBox, previewDPI)
#: settings.ui:253
#, kde-format
msgid "50"
msgstr "50"

#. i18n: ectx: property (text), item, widget (QComboBox, previewDPI)
#: settings.ui:258
#, kde-format
msgid "75"
msgstr "75"

#. i18n: ectx: property (text), item, widget (QComboBox, previewDPI)
#: settings.ui:263
#, kde-format
msgid "100"
msgstr "100"

#. i18n: ectx: property (text), item, widget (QComboBox, previewDPI)
#: settings.ui:268
#, kde-format
msgid "150"
msgstr "150"

#. i18n: ectx: property (text), item, widget (QComboBox, previewDPI)
#: settings.ui:273
#, kde-format
msgid "300"
msgstr "300"

#. i18n: ectx: property (text), widget (QCheckBox, u_disableSelections)
#: settings.ui:286
#, kde-format
msgid "Disable automatic selections"
msgstr "Otomatik seçimleri devre dışı bırak"

#. i18n: ectx: property (text), widget (QPushButton, revertOptions)
#: settings.ui:319
#, kde-format
msgid "Revert scanner options to default values"
msgstr "Tarayıcı seçeneklerini öntanımlı değerlerine geri döndür"

#: skanlite.cpp:57
#, kde-format
msgid "About"
msgstr "Hakkında"

#: skanlite.cpp:59
#, fuzzy, kde-format
#| msgid "Opening the selected scanner failed."
msgid "Reselect scanner device"
msgstr "Seçilen tarayıcı açılamadı."

#: skanlite.cpp:78
#, kde-format
msgid "Saving: %v kB"
msgstr "Kaydediliyor: %v kB"

#: skanlite.cpp:175
#, kde-format
msgid "Skanlite Settings"
msgstr "Skanlite Ayarları"

#: skanlite.cpp:195 skanlite.cpp:294
#, kde-format
msgid "Opening the selected scanner failed."
msgstr "Seçilen tarayıcı açılamadı."

#: skanlite.cpp:309
#, kde-format
msgid "%1: %2"
msgstr "%1: %2"

#: skanlite.cpp:329
#, kde-format
msgctxt "prefix for auto naming"
msgid "Image-"
msgstr "Resim-"

#: skanlite.cpp:470
#, fuzzy, kde-format
#| msgid ""
#| "The image will be saved in the PNG format, as Skanlite only supports "
#| "saving 16 bit color images in the PNG format."
msgid ""
"The image will be saved in the PNG format, as the selected image type does "
"not support saving 16 bit color images."
msgstr ""
"Skanlite sadece PNG biçeminde 16 bit renkli resimleri kaydedebildiğinden "
"resim PNG biçeminde kaydedilecek."

#: skanlite.cpp:495
#, kde-format
msgid "New Image File Name"
msgstr "Yeni Resim Dosya Adı"

#: skanlite.cpp:598
#, kde-format
msgid "Failed to save image"
msgstr "Resim kaydedilemedi.."

#: skanlite.cpp:611
#, kde-format
msgid "Failed to upload image"
msgstr "Resim yükleme başarısız"

#: skanlite.cpp:776
#, kde-format
msgctxt "@title:window %1 = scanner maker, %2 = scanner model"
msgid "%1 %2 - Skanlite"
msgstr "%1 %2 - Skanlite"

#: skanlite.cpp:778
#, kde-format
msgctxt "@title:window %1 = scanner device"
msgid "%1 - Skanlite"
msgstr "%1 - Skanlite"

#~ msgid "..."
#~ msgstr "..."

#~ msgid "Do you want to overwrite \"%1\"?"
#~ msgstr "\"%1\" üzerine yazmak istiyor musunuz?"

#~ msgid "Overwrite"
#~ msgstr "Üzerine Yaz"

#~ msgid "Save"
#~ msgstr "Kaydet"
